<?php

namespace Drupal\pepper_revisions\Plugin\GraphQL\SchemaExtension;

use Drupal\Core\Language\Language;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\graphql\GraphQL\ResolverBuilder;
use Drupal\graphql\GraphQL\ResolverRegistry;
use Drupal\graphql\GraphQL\ResolverRegistryInterface;
use Drupal\graphql\Plugin\GraphQL\SchemaExtension\SdlSchemaExtensionPluginBase;
use Drupal\media\Entity\Media;
use Drupal\node\Entity\Node;
use Drupal\pepper_graphql\Plugin\GraphQL\DataProducer\PepperRouteLoad;
use GraphQL\Error\Error;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Schema extension.
 *
 * @SchemaExtension (
 *   id = "pepper_revisions",
 *   description = "Implements the schema for the pepper revisions module.",
 *   name = "Pepper revisions schema",
 *   schema = "custom_composable"
 * )
 */
class PepperRevisionsExtension extends SdlSchemaExtensionPluginBase implements ContainerFactoryPluginInterface {

  /**
   * Maps the schema languageId ENUM to Drupal language ids.
   *
   * Note! This is not the mapping to the path prefix itself. Here we only map
   * the incoming ENUM from GraphQL to the language id of Drupal.
   *
   * @var array
   *
   * @see PepperRouteLoad::getPathPrefix
   */
  private $languageIdMapping = [
    'EN' => 'en',
    'DE' => 'de',
    'FR' => 'fr',
    'IT' => 'it',
  ];

  /**
   * The language manager service.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  private $languageManager;

  /**
   * {@inheritdoc}
   *
   * @codeCoverageIgnore
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('module_handler'),
      $container->get('language_manager')
    );
  }

  /**
   * HeadlessSchema constructor.
   *
   * @param array $configuration
   *   The plugin configuration.
   * @param string $pluginId
   *   The plugin Id.
   * @param mixed $pluginDefinition
   *   The plugin definition.
   * @param \Drupal\Core\Cache\CacheBackendInterface $astCache
   *   The cache bin for caching the parsed SDL.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $moduleHandler
   *   The module handler service.
   * @param \Drupal\graphql\Plugin\SchemaExtensionPluginManager $extensionManager
   *   The schema extension plugin manager.
   * @param array $config
   *   The service configuration.
   * @param \Drupal\Core\Language\LanguageManagerInterface $languageManager
   *   The language manager service.
   */
  public function __construct(
    array $configuration,
    $pluginId,
    $pluginDefinition,
    ModuleHandlerInterface $moduleHandler,
    LanguageManagerInterface $languageManager
  ) {
    parent::__construct($configuration, $pluginId, $pluginDefinition, $moduleHandler, $languageManager);
    $this->languageManager = $languageManager;
  }

  public function registerResolvers(ResolverRegistryInterface $registry) {
    $builder = new ResolverBuilder();
    $this->addFieldResolvers($registry, $builder);
  }

  public function addFieldResolvers($registry, $builder) {
    $registry->addFieldResolver('Query', 'revision',
      $builder->produce('pepper_revision_load')
        ->map('revision_id', $builder->fromArgument('revision_id'))
        // Map the enum values from frontend to drupal langcodes.
        ->map('language', $builder->callback(function ($value, $args) {

          // Allow empty language parameter, as its optional.
          if (empty($args['language'])) {
            return '';
          }

          if (isset($this->languageIdMapping[$args['language']])) {
            return $this->languageIdMapping[$args['language']];
          }

          throw new Error('Could not resolve language.');

        }))
    );
  }

}
