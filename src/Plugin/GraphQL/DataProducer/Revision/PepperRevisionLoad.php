<?php

namespace Drupal\pepper_revisions\Plugin\GraphQL\DataProducer\Revision;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Site\Settings;
use Drupal\graphql\GraphQL\Execution\FieldContext;
use Drupal\graphql\Plugin\GraphQL\DataProducer\DataProducerPluginBase;
use Drupal\node\Entity\Node;
use Drupal\pepper_graphql\Wrapper\Routing\EntityResponse;
use Drupal\pepper_graphql\Wrapper\Routing\ErrorResponse;
use Drupal\redirect\RedirectRepository;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Data producer for route loads.
 *
 * @DataProducer(
 *   id = "pepper_revision_load",
 *   name = @Translation("Load revision"),
 *   description = @Translation("Loads an revision entity."),
 *   produces = @ContextDefinition("any",
 *     label = @Translation("Response")
 *   ),
 *   consumes = {
 *     "revision_id" = @ContextDefinition("integer",
 *       label = @Translation("uuid")
 *     ),
 *     "language" = @ContextDefinition("string",
 *       label = @Translation("Language id"),
 *       default_value = ""
 *     )
 *   }
 * )
 */
class PepperRevisionLoad extends DataProducerPluginBase implements ContainerFactoryPluginInterface {

  /**
   * The path validator service.
   *
   * @var \Drupal\Core\Path\PathValidatorInterface
   */
  protected $pathValidator;

  /**
   * Module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * Redirect repository.
   *
   * @var \Drupal\redirect\RedirectRepository|null
   */
  protected $redirectRepository;

  /**
   * Entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   *
   * @codeCoverageIgnore
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('redirect.repository', ContainerInterface::NULL_ON_INVALID_REFERENCE)
    );
  }

  /**
   * RouteLoad constructor.
   *
   * @param array $configuration
   *   The plugin configuration.
   * @param string $pluginId
   *   The plugin id.
   * @param mixed $pluginDefinition
   *   The plugin definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager service.
   * @param \Drupal\redirect\RedirectRepository|null $redirectRepository
   *   The redirect repository service if the module is enabled.
   */
  public function __construct(
    array $configuration,
    $pluginId,
    $pluginDefinition,
    EntityTypeManagerInterface $entityTypeManager,
    RedirectRepository $redirectRepository = NULL
  ) {
    parent::__construct($configuration, $pluginId, $pluginDefinition);
    $this->entityTypeManager = $entityTypeManager;
    $this->redirectRepository = $redirectRepository;
  }

  /**
   * Resolve method.
   *
   * @param int $revision_id
   *   Uuid to preview.
   * @param string $language
   *   The language id to get the content for.
   * @param \Drupal\graphql\GraphQL\Execution\FieldContext $context
   *   Field context.
   *
   * @return \GraphQL\Deferred|\Drupal\pepper_graphql\Wrapper\Routing\RouteResponseInterface
   *   A promise or a response.
   *
   * @throws \GraphQL\Error\Error
   */
  public function resolve($revision_id, $language, FieldContext $context) {
    // Check the user access first.
    $cookie_domain_enabled = Settings::get('pepper_preview.cookie_domain_override');
    if (!empty($cookie_domain_enabled)) {
      // Check if User has permission to view unpublished content.
      if (!\Drupal::currentUser()->hasPermission('pepper preview functionality')) {
        // Permission denied.
        $login_url = \Drupal::request()->getSchemeAndHttpHost() . "/user/login";
        return new ErrorResponse(
          403,
          'Permission denied. To access the preview functionality, please log in to Drupal first: <a href="' . $login_url . '" target="_blank">' . $login_url . '</a>'
        );
      }
    }
    $revision = $this->entityTypeManager->getStorage('node')->loadRevision($revision_id);
    if (!$revision) {
      return new ErrorResponse(
        404,
        'No Revision with revision id ' . $revision_id . ' found!'
      );
    }

    if ($revision instanceof Node) {
      // Check if it is the correct language.
      if ($revision->isTranslatable()) {
        // Try to load the revision in the correct language.
        try {
          $revision = $revision->getTranslation($language);
        }
        catch(\Exception $exception) {
          return new ErrorResponse(
            404,
            'No Revision with revision id ' . $revision_id . ' for language ' . $language . ' found!'
          );
        }
      }
      return new EntityResponse($revision);
    }

    // @TODO Define other outcomes.
    return new ErrorResponse(
      400,
      'General error.'
    );
  }

}
