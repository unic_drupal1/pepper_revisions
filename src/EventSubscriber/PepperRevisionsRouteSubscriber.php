<?php

namespace Drupal\pepper_revisions\EventSubscriber;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;

/**
 * Class PepperRevisionsRouteSubscriber.
 *
 * Overwrites the revision history controller.
 */
class PepperRevisionsRouteSubscriber extends RouteSubscriberBase {

  /**
   * Alters the available routes.
   *
   * @param \Symfony\Component\Routing\RouteCollection $collection
   *   The RouteCollection.
   *
   * @return void
   *   No return.
   */
  protected function alterRoutes(RouteCollection $collection) {
    if ($route = $collection->get('entity.node.version_history')) {
      $route->setDefault('_controller', 'Drupal\pepper_revisions\Controller\PepperRevisionsController::revisionOverview');
    }
  }

}
